#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include "slisersettings.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void widthEdit_textEdited(const QString &text);
    void on_cutModelButton_clicked();
	void on_browseButton_clicked();
	void on_createArrayButton_clicked();
    //void on_testButton_clicked();
	void on_calculateButton_clicked();
	void on_cutAxisCombo_currentIndexChanged(int index);

 //   void on_pushButton_clicked();

    void on_calculateVolumeButton_clicked();

private:
	Ui::MainWindow *ui;
	QList<QLabel *> slicesNumbersLabels;
    QList<QLineEdit *> widthsEdits;
	QList<QCheckBox *> fixedChecks;


    double fullVolume;

    void createWidthsTable();
    bool getModelSize();
    bool getVolume(QString scad_output, int i);
    void calculateWidth();
    void calculateVolume();
    void cut(double, double, int);
    void writeToLog(const QString &s);


    double Size;
    QMap<int, double> tmpVolume;
    QMap<int, double> sliceVolume; // масив, в який зберігаються обчислення об'ємів
    QMap<QString, float> modelSize;
    QMap<QString, QString> settings;
    QMap<int, double>fixSlices;
    QVector<double> tmpPoints;

};

#endif // MAINWINDOW_H
