#include "mainwindow.h"
#include <QApplication>
#define VERSION "1.4.4"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    QApplication::setApplicationName("3D Model Cut  " VERSION);

// Under Windows make the interface font slightly biger:
#ifdef Q_OS_WIN
	QFont f(qApp->font());
	f.setPointSize(f.pointSize() + 3);
	qApp->setFont(f);
#endif

	MainWindow w;
	w.show();
	return a.exec();
}
