﻿#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QProcess>
#include <QDebug>
#include <iostream>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "cmath"


// Constructor:
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	slicesNumbersLabels(),
	widthsEdits(),
	fixedChecks(),
	settings()
{
	ui->setupUi(this);
	setWindowTitle(QApplication::applicationName());

//    slicerSettings = new SliserSettings (QApplication::applicationDirPath());


    // Load settings:
    QString filePath(QApplication::applicationDirPath() + "/settings.ini"), s; int i;
    QFile file(filePath);
    if (file.open(QFile::ReadOnly | QFile::Text) == false) {
        QMessageBox::warning(0, QApplication::applicationName(),
            "Open error of \"" + QDir::toNativeSeparators(filePath) +
            "\" file for read.");
        return;
    }
    while (file.atEnd() == false) {
        s = QString::fromUtf8(file.readLine()).trimmed();
        if (s.isEmpty()) continue;
        if ((i = s.indexOf('=')) != -1 && s.left(i) != "Fix_slice") settings[s.left(i)] = s.mid(i + 1);
        if ((i = s.indexOf('=')) != -1 && s.left(i) == "Fix_slice") {    //condition for downloading fixed slises
            s = s.mid(i + 1);
            if ((i = s.indexOf(',')) != -1)
                fixSlices[s.left(i).toInt() - 1] = s.mid(i+1).toDouble();
        }
    }
    file.close();


    ui->numberOfSlicesEdit->setText(settings["defualtSlices"]);

	if ((i = ui->cutAxisCombo->findText(settings["defualtCutAxis"])) != -1)
		ui->cutAxisCombo->setCurrentIndex(i);

    createWidthsTable();

    // Для тесту:
    ui->modelFilePathEdit->setText(
		QDir::toNativeSeparators(QApplication::applicationDirPath() + "/torus.stl"));


    if (!getModelSize()) QMessageBox::warning(0, QApplication::applicationName(),
                                                       "Can`t calculate model size");
    //!!!перевірити правопис

    on_calculateButton_clicked();
//    ui ->label_7->setText(settings["precision"]);

}

MainWindow::~MainWindow() // Destructor.
{
	delete ui;
}

// The createWidthsTable function dynamicaly creates the table with slices numbers,
// widths editors and checkboxes.
void MainWindow::createWidthsTable()
{
    QLabel *label; QLineEdit *edit; QCheckBox *check; int i;
	for (i = 0; i < ui->numberOfSlicesEdit->text().toInt(); i++) {
		label = new QLabel(QString::number(i + 1), this);
		ui->widthsGridLayout->addWidget(label, 0, i + 1);
		slicesNumbersLabels.append(label);
		edit = new QLineEdit(this);
		ui->widthsGridLayout->addWidget(edit, 1, i + 1);
		widthsEdits.append(edit);
        check = new QCheckBox(this);
		ui->widthsGridLayout->addWidget(check, 2, i + 1);
		fixedChecks.append(check);
		connect(edit, SIGNAL(textEdited(const QString &)),
				this, SLOT(widthEdit_textEdited(const QString &)));
	}

    for (int i = 0; i < slicesNumbersLabels.count(); i++) {
        if(fixSlices[i] != 0){
        fixedChecks[i] -> setCheckState(Qt::Checked);
        widthsEdits[i] -> setText(QString::number(fixSlices[i]));
        }
    }

 }



bool MainWindow::getVolume(QString scad_output,int i) {
    QStringList parameters; QProcess process; int r;
    QString standardOutput, standardError;


    if (!settings.contains("admeshExecutableFilePath")) {
        QMessageBox::warning(this, QApplication::applicationName(),
            "There is no admeshExecutableFilePath in the settings.");
        return false;
    }

    parameters.append(QApplication::applicationDirPath() + "/" + scad_output + "/r" + QString::number(i + 1) + ".stl");
    process.start(settings["admeshExecutableFilePath"], parameters);
    process.waitForFinished(-1);
    r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
    standardOutput = process.readAllStandardOutput(),
    standardError = process.readAllStandardError();
    if (r) {
        QMessageBox::warning(this, QApplication::applicationName(),
            "admesh exit code: " + QString::number(r) +
            "\nadmesh result:\n" + standardOutput + standardError);
        return false;
    }




    QRegExp rx(
        ".*Volume   :  ([-.\\d ]*)");

    if (rx.indexIn(standardOutput.remove('\r')) == -1) {
        QMessageBox::warning(this, QApplication::applicationName(),
                             "Could not parse the admesh output.\n" + standardOutput);
        return false;
    }

    tmpVolume[i] = rx.cap(1).trimmed().toDouble();
//    qDebug() << "tmpVolume" << i << " = " << tmpVolume[i];


    return true;

}


// QMap<QString, Double> &size - returns size list. Uses for that Admesh external console application
// minX, size["minX"], size["maxX"], size["minY"], size["maxY"], size["minZ"], size["maxZ"]


bool MainWindow::getModelSize() { // Returns false on error.
	QStringList parameters; QProcess process; int r;
	QString standardOutput, standardError;

	ui->modelSizeXEdit->setText("");
	ui->modelSizeYEdit->setText("");
	ui->modelSizeZEdit->setText("");

	if (!settings.contains("admeshExecutableFilePath")) {
		QMessageBox::warning(this, QApplication::applicationName(),
			"There is no admeshExecutableFilePath in the settings.");
		return false;
	}

    parameters.append(ui->modelFilePathEdit->text());
    process.start(settings["admeshExecutableFilePath"], parameters);
    process.waitForFinished(-1);
    r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
    standardOutput = process.readAllStandardOutput(),
	standardError = process.readAllStandardError();
    if (r) {
		QMessageBox::warning(this, QApplication::applicationName(),
			"admesh exit code: " + QString::number(r) +
			"\nadmesh result:\n" + standardOutput + standardError);
		return false;
	}

	QRegExp rx(
		"\n=+ Size =+\n"
		"Min X = ([-.\\d ]+), Max X = ([-.\\d ]+)\n"
		"Min Y = ([-.\\d ]+), Max Y = ([-.\\d ]+)\n"
        "Min Z = ([-.\\d ]+), Max Z = ([-.\\d ]+)\n"
        ".*Volume   :  ([-.\\d ]*)");

	if (rx.indexIn(standardOutput.remove('\r')) == -1) {
		QMessageBox::warning(this, QApplication::applicationName(),
							 "Could not parse the admesh output.\n" + standardOutput);
		return false;
	}

    //calculating general volume
    fullVolume = rx.cap(7).trimmed().toDouble();

    //calculating other
    modelSize["minX"] = rx.cap(1).trimmed().toFloat();
    modelSize["maxX"] = rx.cap(2).trimmed().toFloat();
    modelSize["minY"] = rx.cap(3).trimmed().toFloat();
    modelSize["maxY"] = rx.cap(4).trimmed().toFloat();
    modelSize["minZ"] = rx.cap(5).trimmed().toFloat();
    modelSize["maxZ"] = rx.cap(6).trimmed().toFloat();
    ui->modelSizeXEdit->setText(QString::number(modelSize["maxX"] - modelSize["minX"]));
    ui->modelSizeYEdit->setText(QString::number(modelSize["maxY"] - modelSize["minY"]));
    ui->modelSizeZEdit->setText(QString::number(modelSize["maxZ"] - modelSize["minZ"]));
/*
    QStringList list; QMapIterator<QString, Double> iterator(modelSize);
	while (iterator.hasNext()) {
		iterator.next();
		list.append(iterator.key() + "=" + QString::number(iterator.value()));
	}
	QMessageBox::information(this, QApplication::applicationName(), list.join("\n"));
*/
	return true;
}

// Turn the checkbox on when user change the slice width:
void MainWindow::widthEdit_textEdited(const QString & /* text */) {
	QObject *s = sender(); int i;
	if (s == NULL) return;
	if ((i = widthsEdits.indexOf((QLineEdit*)s)) != -1)
		fixedChecks[i]->setCheckState(Qt::Checked);
}

void MainWindow::on_browseButton_clicked()
{
    tmpVolume.clear();
    sliceVolume.clear();
    fullVolume = 0;

	QString filePath(QFileDialog::getOpenFileName(
		this, "Select stl file", "", "stl files (*.stl);;All files (*.*)"));
	if (!filePath.isEmpty()) {
        ui->modelFilePathEdit->setText(QDir::toNativeSeparators(filePath));
        if (!getModelSize()) QMessageBox::warning(0, QApplication::applicationName(),
                                                           "Can`t calculate model size");

        on_calculateButton_clicked();

	}
}

void MainWindow::on_cutAxisCombo_currentIndexChanged(int /* index */ )
{

    on_calculateButton_clicked();
}

void MainWindow::on_createArrayButton_clicked()
{
	// Remove old elements from widhts table:
	for (int i = 0; i < slicesNumbersLabels.count(); i++) {
		ui->widthsGridLayout->removeWidget(slicesNumbersLabels[i]);
		delete slicesNumbersLabels[i];
		ui->widthsGridLayout->removeWidget(widthsEdits[i]);
		delete widthsEdits[i];
		ui->widthsGridLayout->removeWidget(fixedChecks[i]);
		delete fixedChecks[i];
	}
	slicesNumbersLabels.clear();
	widthsEdits.clear();
	fixedChecks.clear();

	createWidthsTable();

    on_calculateButton_clicked();
}
/*
QString MainWindow::prepareDouble(QString f)
{
   return QString::number((int)(f.toDouble() * 1000));
}

QString MainWindow::prepareDouble(Double f)
{
   return QString::number((int)(f * 1000));
}
*/

void MainWindow::writeToLog(const QString &s) {
	QString filePath(QApplication::applicationDirPath() + "/log.txt");
	QFile file(filePath);
	if (!file.open(QFile::Append)) {
		QMessageBox::warning(this, QApplication::applicationName(),
			"Error of open the \"" +
			QDir::toNativeSeparators(filePath) + "\" file for write.");
		return;
	}
	file.write(s.toUtf8());
	file.close();
}

/*void MainWindow::on_testButton_clicked()
{
	if (!QDir::setCurrent(QApplication::applicationDirPath())) return;

	QDir dir(QApplication::applicationDirPath());
	if (!dir.exists("scad_output")) {
		if (!dir.mkdir("scad_output")) {
			QMessageBox::warning(this, QApplication::applicationName(),
				"Could not make " +
				QDir::toNativeSeparators(QApplication::applicationDirPath() +
					"/scad_output") + " directory.");
			return;
		}
	}

	QStringList parameters, a; QProcess process; int i, r;
	QString standardOutput, standardError;

//	ui->testButton->setEnabled(false);
	qApp->processEvents();
	for (i = 1; i <= 6; i++) {
		parameters.clear();
		parameters.append("-o");
        parameters.append(QString("scad_output") + QDir::separator() + "r" +
						  QString::number(i) + ".stl");
		parameters.append("-D user_file=\"torus.stl\"");
		parameters.append("-D p_s_slice_size=\"0,159,200,650,800\"");
		parameters.append("-D p=\"" + QString::number(i) + "\"");
		parameters.append("slicer.scad");

		process.start(settings["openscadExecutableFilePath"], parameters);
		process.waitForFinished(-1); // will wait forever until finished
		r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
		standardOutput = process.readAllStandardOutput(),
		standardError = process.readAllStandardError();

		QMessageBox::information(this, QApplication::applicationName(),
"openscad parameters:\n" + parameters.join("\n") +
"\n-----------------------------------------------------------\nopenscad exit code: " +
QString::number(r) + "\nopenscad result:\n" + standardOutput + standardError);
	}
//	ui->testButton->setEnabled(true);
}*/

void MainWindow::on_calculateButton_clicked()
{
    ui->cutModelButton->setEnabled(false);
    ui->calculateButton->setEnabled(false);
    ui->calculateVolumeButton->setEnabled(false);

    double modelSize, fixedWidthsSum = 0;
    int i, n = 0; // n is a number of chackboxes that are turned on.
    if (ui->modelSizeXEdit->text().isEmpty()) return;
    modelSize = ((ui->cutAxisCombo->currentText() == "x") ?
        ui->modelSizeXEdit->text().toDouble() : ui->modelSizeYEdit->text().toDouble());

    for (i = 0; i < fixedChecks.count(); i++) {
        if (fixedChecks[i]->checkState() == Qt::Checked) {
            n++;
            fixedWidthsSum += widthsEdits[i]->text().toDouble();
        }
    }

    for (i = 0; i < widthsEdits.count(); i++) {
        if (n) {
            if (fixedChecks[i]->checkState() == Qt::Unchecked)
                widthsEdits[i]->setText(QString::number(
                    (modelSize - fixedWidthsSum) / (widthsEdits.count() - n)));
        } else
            widthsEdits[i]->setText(QString::number(modelSize / widthsEdits.count()));
    }

    ui->calculateVolumeButton->setEnabled(true);
    ui->cutModelButton->setEnabled(true);
    ui->calculateButton->setEnabled(true);
}

void MainWindow::on_cutModelButton_clicked()
{
    if (!settings.contains("openscadExecutableFilePath")) {
        QMessageBox::warning(this, QApplication::applicationName(),
            "There is no openscadExecutableFilePath in the settings.");
        return;
    }
    if (ui->modelFilePathEdit->text().isEmpty()) {
        QMessageBox::warning(this, QApplication::applicationName(),
                             "The model file path is not given.");
        return;
    }
/*
    QProcess::startDetached("openscad");
    QString s("cd D:/3d_slicer/scad /n openscad -o ../scad_output/r%G.stl -D \"user_file=\\\"torus.stl\\\"\" -D \"p_s_slice_size=\\\"0,159,200,650,800\\\"\" -D \"p=\\\"%G\\\"\" slicer.scad");
    const char[] s = "cd D:/3d_slicer/scad";
    system(s);
*/
    QStringList parameters, a; QProcess process; int i, r;
    QString standardOutput, standardError;
    QMap<QString, QString> definitions;
    // definitions will be passed to openscad after -D parametr.


    if (!QDir::setCurrent(QApplication::applicationDirPath())) return;

    // Make the scad_output directory if it does not exist:
    QDir dir(QApplication::applicationDirPath());
    if (!dir.exists("scad_output")) {
        if (!dir.mkdir("scad_output")) {
            QMessageBox::warning(this, QApplication::applicationName(),
                "Could not make " +
                QDir::toNativeSeparators(QApplication::applicationDirPath() +
                    "/scad_output") + " directory.");
            return;
        }
    }

    ui->cutModelButton->setEnabled(false);
    qApp->processEvents();

    definitions["user_file"] =
        "\"" + ui->modelFilePathEdit->text().replace("\\", "\\\\") + "\"";
    /*
    definitions["u_s_x"] =
        QString::number((int)(ui->modelSizeXEdit->text().toFloat() * 10));
    definitions["u_s_y"] =
        QString::number((int)(ui->modelSizeYEdit->text().toFloat() * 10));
    definitions["u_s_z"] =
        QString::number((int)(ui->modelSizeZEdit->text().toFloat() * 10));
    */

    definitions["u_s_x"] = QString::number(((modelSize["maxX"] - modelSize["minX"])));
    definitions["u_s_y"] = QString::number(((modelSize["maxY"] - modelSize["minY"])));
    definitions["u_s_z"] = QString::number(((modelSize["maxZ"] - modelSize["minZ"])));
    definitions["axis"] = "\"" + ui->cutAxisCombo->currentText() + "\"";

//	qDebug() << "axis: " << ui->cutAxisCombo->currentText();
    if (ui->cutAxisCombo->currentText() == "x")
    {
//		qDebug() << "axis: X";
        definitions["u_min"] = QString::number((modelSize["minX"]));
        definitions["u_max"] = QString::number((modelSize["maxX"]));
    }
    else //definitions["axis"] == "\"y\""
    {
//		qDebug() << "axis: Y";
        definitions["u_min"] = QString::number((modelSize["minY"]));
//		qDebug() << "minY: "<< definitions["u_min"];
        definitions["u_max"] = QString::number((modelSize["maxY"]));
    }

    definitions["n"] = QString::number(widthsEdits.count());
    //definitions["u_mi_x"] = QString::number((int)(modelSize["minX"]*1000));
    //definitions["u_mi_y"] = QString::number((int)(modelSize["minY"]*1000));
    //definitions["u_mi_z"] = QString::number((int)(modelSize["minZ"]*1000));
    //definitions["u_ma_x"] = QString::number((int)(modelSize["maxX"])*1000);
    //definitions["u_ma_y"] = QString::number((int)(modelSize["maxY"])*1000);
    //definitions["u_ma_z"] = QString::number((int)(modelSize["maxZ"])*1000);

    float xi = 0, size_sum = 0; int sum_left = 0, w;
    //!!! початок рудиментарного коду (який не буде потрібен у наступній версії)
    a.append("0");
    for (i = 0; i < widthsEdits.count(); i++)
        size_sum += widthsEdits[i]->text().toFloat();
    for (i = 0; i < widthsEdits.count(); i++) {
        xi = widthsEdits[i]->text().toFloat();
        w = (int)(xi/size_sum*1000); //обчислюємо відносну величину
        //шматка - відносно розміру моделі по відповідній осі
        sum_left += + w; // обчислюємо точки порізки по тій відносній величині
        a.append(QString::number(sum_left));
    }
    definitions["p_s_slice_size"] = "\"" + a.join(",") + "\"";
    //Обчислення цього масиву - рудиментарне і пізніше ми його заберемо.

    QString logFilePath(QApplication::applicationDirPath() + "/log.txt");
    if (QFile::exists(logFilePath)) {
        if (!QFile::remove(logFilePath)) {
            QMessageBox::warning(this, QApplication::applicationName(),
                "Could not remove the " + QDir::toNativeSeparators(logFilePath) +
                " file.");
        }
    }

    double x0, x1;
    size_sum = 0;
    for (i = 1; i <= widthsEdits.count(); i++) {
        if (i > 1) writeToLog(
            "----------------------------------------------------------------\n");

        parameters.clear();
        parameters.append("-o");

        //порахуємо ліву та праву межу в сантиметрах від лівого краю моделі.
        x0 = size_sum;
        size_sum += widthsEdits[i - 1]->text().toDouble();
        x1 = size_sum;
        //відповідно Х0 та Х1 будуть відрізнятись на довжину поточного відрізку.
        //і додамо їх тепер у параметри
        definitions["x0"] = QString::number(x0);
        definitions["x1"] = QString::number(x1);

        parameters.append(QString("scad_output") + QDir::separator() + "r" +
                          QString::number(i) + ".stl");
        definitions["p"] = "\"" + QString::number(i) + "\"";
        // The p definition is needed as string.

        {
            QMapIterator<QString, QString> iterator(definitions);
            while (iterator.hasNext()) {
                iterator.next();
                parameters.append("-D " + iterator.key() + "=" + iterator.value());
            }
        }

        parameters.append("slicer.scad");
        writeToLog("openscad parameters:\n" + parameters.join(" ") + "\n");

        // Start openscad and wait finish:
        process.start(settings["openscadExecutableFilePath"], parameters);
        process.waitForFinished(-1); // will wait forever until finished
        r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
        standardOutput = process.readAllStandardOutput(),
        standardError = process.readAllStandardError();

        writeToLog("\nVolume slice:  " + QString::number(sliceVolume[i - 1]) + "\n");
//!!! потрібно буде поправити запис обєму в лог файл так, щоб коли обчислення відбув. не по обєму, то він не виводивя.
        writeToLog(
            "openscad result:\n"  + (standardOutput + standardError).trimmed() + "\n");
        if (r) {
            QMessageBox::warning(this, QApplication::applicationName(),
"openscad parameters:\n" + parameters.join("\n") +
"\n-----------------------------------------------------------\nopenscad exit code: " +
QString::number(r) + "\nopenscad result:\n" + standardOutput + standardError);
            break;
        }
//		break;
    }
    ui->cutModelButton->setEnabled(true);

    if (!r) QMessageBox::information(this, QApplication::applicationName(), "Done.");

}


void MainWindow::cut(double x0, double x1, int i)
{
    QString scadOutput = "tmp";

    qDebug() << "x0 = " << x0 << "x1 = " << x1;



    QStringList parameters, a; QProcess process; int r;
    QString standardOutput, standardError;
    QMap<QString, QString> definitions;
    // definitions will be passed to openscad after -D parametr.


    if (!QDir::setCurrent(QApplication::applicationDirPath())) return;

    // Make the scad_output directory if it does not exist:
    QDir dir(QApplication::applicationDirPath());
    if (!dir.exists(scadOutput)) {
        if (!dir.mkdir(scadOutput)) {
            QMessageBox::warning(this, QApplication::applicationName(),
                                 "Could not make " +
                                 QDir::toNativeSeparators(QApplication::applicationDirPath() +
                    scadOutput) + " directory.");
            return;
        }
    }

    ui->cutModelButton->setEnabled(false);
    qApp->processEvents();

    definitions["user_file"] =
            "\"" + ui->modelFilePathEdit->text().replace("\\", "\\\\") + "\"";


    definitions["u_s_x"] = QString::number(((modelSize["maxX"] - modelSize["minX"])));
    definitions["u_s_y"] = QString::number(((modelSize["maxY"] - modelSize["minY"])));
    definitions["u_s_z"] = QString::number(((modelSize["maxZ"] - modelSize["minZ"])));
    definitions["axis"] = "\"" + ui->cutAxisCombo->currentText() + "\"";


    if (ui->cutAxisCombo->currentText() == "x")
    {
        definitions["u_min"] = QString::number((modelSize["minX"]));
        definitions["u_max"] = QString::number((modelSize["maxX"]));
    }
    else
    {
        definitions["u_min"] = QString::number((modelSize["minY"]));
        definitions["u_max"] = QString::number((modelSize["maxY"]));
    }

    definitions["n"] = QString::number(widthsEdits.count());

    float xi = 0; int sum_left = 0, w;


    //!!! початок рудиментарного коду (який не буде потрібен у наступній версії)

    a.append("0");

    QVector<double>::iterator j;


    int n;
    for (n = 0,j = tmpPoints.begin(); n < widthsEdits.count(); n++, j++) {
        xi = *(j+1) - *j;
        w = (int)(xi/Size*1000); //обчислюємо відносну величину
        //шматка - відносно розміру моделі по відповідній осі
        sum_left += +w; // обчислюємо точки порізки по тій відносній величині
        a.append(QString::number(sum_left));
    }


    definitions["p_s_slice_size"] = "\"" + a.join(",") + "\"";
    //Обчислення цього масиву - рудиментарне і пізніше ми його заберемо.



    parameters.clear();
    parameters.append("-o");


    definitions["x0"] = QString::number(x0);
    definitions["x1"] = QString::number(x1);

    parameters.append(QString(scadOutput) + QDir::separator() + "r" +
                      QString::number(i + 1) + ".stl");
    definitions["p"] = "\"" + QString::number(i + 1) + "\"";
    // The p definition is needed as string.

    {
        QMapIterator<QString, QString> iterator(definitions);
            while (iterator.hasNext()) {
                iterator.next();
                parameters.append("-D " + iterator.key() + "=" + iterator.value());
            }
    }

    parameters.append("slicer.scad");

    // Start openscad and wait finish:
    process.start(settings["openscadExecutableFilePath"], parameters);
    process.waitForFinished(-1); // will wait forever until finished
    r = ((process.exitStatus() == QProcess::NormalExit) ? process.exitCode() : -1);
    standardOutput = process.readAllStandardOutput(),
            standardError = process.readAllStandardError();

    if (r) {
        QMessageBox::warning(this, QApplication::applicationName(),
                             "openscad parameters:\n" + parameters.join("\n") +
                             "\n-----------------------------------------------------------\nopenscad exit code: " +
                             QString::number(r) + "\nopenscad result:\n" + standardOutput + standardError);

    }


}

void MainWindow::on_calculateVolumeButton_clicked()
{
    ui->cutModelButton->setEnabled(false);
    ui->calculateButton->setEnabled(false);
    ui->calculateVolumeButton->setEnabled(false);





    double precision = settings["precision"].toDouble();
    int i, W = 0, F = 0;


    QVector<double> cutPoints;
    QVector<double>::iterator j;
    QVector<double>::iterator z;

    double x0, x1, n0, n1;
    double fixWidthSum = 0;



    Size = ((ui->cutAxisCombo->currentText() == "x") ?
        ui->modelSizeXEdit->text().toDouble() : ui->modelSizeYEdit->text().toDouble());

    for (i = 0; i < fixedChecks.count(); i++)
        if (fixedChecks[i]->checkState() == Qt::Checked){
            F++;
            fixWidthSum += widthsEdits[i]->text().toDouble();
        }

    if (fixWidthSum >= Size)
    {
        QMessageBox::warning(this, QApplication::applicationName(),
                             "Error :: width fixed slises"); // перевірити граматику

        ui->cutModelButton->setEnabled(true);
        ui->calculateButton->setEnabled(true);
        ui->calculateVolumeButton->setEnabled(true);
        return;
    }


    if (F == widthsEdits.count())
    {
        QMessageBox::warning(this, QApplication::applicationName(),
                             "All slices is fixed"); // перевірити граматику

        ui->cutModelButton->setEnabled(true);
        ui->calculateButton->setEnabled(true);
        ui->calculateVolumeButton->setEnabled(true);
        return;
    }


    W = widthsEdits.count() - F;  //the number is not fixed parts

    double slicePrecision = precision/W;



    //calculating volume for slice
    double equalVolume = 0, equalSize = 0;

    equalVolume = fullVolume/W;
    qDebug() << "W  " << W;


    //зробити перевірку, чи модель взагалі подільна
    //вивести на форму середній об’єм?

    //  що відносно похибок

    x0 = 0;
    x1 = Size;
    n0 = 0;
    n1 = Size;
    double ser = 0;


    cutPoints.append(x0);
    //cutPoints[0] = ser;



    for (i = 0; i < W - 1; i ++) {
    //перший етап порізки

    while(fabs(equalVolume - tmpVolume[i]) >  precision) // має ще бути обмеження по максимальній точності по ширині
    {

        ser = (n0 + n1)/2;
        qDebug() << "ser  " << ser;
        cut(x0, ser, i);

        getVolume("tmp", i);

        qDebug() << "delta" << fabs(equalVolume - tmpVolume[i]);
        qDebug() << "n0  " << n0;
        qDebug() << "n1  " << n1;

        if(equalVolume > tmpVolume[i])
        n0 = ser;
        else  n1 = ser;
    }
    x0 = n0 = ser;
    n1 = Size;
    cutPoints.append(ser);
    sliceVolume[i] = tmpVolume[i];
    }


    //the last step of the algorithm
    cut(x0, Size, W - 1);
    getVolume("tmp", W - 1);
    sliceVolume[i] = tmpVolume[i];
    cutPoints.append(Size);
    //the last step of the algorithm


    //ui->label_7->setText("Delta Relative:  " + QString::number(i));

    //закінчення першого етапу порізки

    qDebug() << "cutPoints" << cutPoints;
    qDebug() << "equalVolume  " << equalVolume;


    //наступний крок. Встановлюємо фіксовані шматочки.



//    cutPoints.clear();

//    equalSize = Size/W;


//    for(i = 0;i < W;i ++)
//        cutPoints.append(i*equalSize);  //просто розміщуємо по рівних частинах

//    cutPoints.append(Size);


    qDebug() << "cutPoints" << cutPoints;


    tmpPoints = cutPoints;

    QVectorIterator<double> k(tmpPoints);

    double tmpW = 0;

    int s;


    double max = 0, maxFixWidthSum = 0;

    for (i = 0; i < fixedChecks.count(); i++){
        if(fixedChecks[i]->checkState() == Qt::Checked) {
            while(i < widthsEdits.count() && fixedChecks[i]->checkState() == Qt::Checked)
            {
                maxFixWidthSum += widthsEdits[i]->text().toDouble();
                i++;
            }
            if (max < maxFixWidthSum) max = maxFixWidthSum;
            maxFixWidthSum = 0;
        }

    }

    qDebug() << "max" << max;


    double minVolumeWidth = Size;

    for(i = 0;i < W;i ++)
       if(tmpPoints[i+1] - tmpPoints[i] < minVolumeWidth)
           minVolumeWidth = tmpPoints[i+1] - tmpPoints[i];

    qDebug() << "minVolumeWidth" << minVolumeWidth;


    if (max < minVolumeWidth)
    {
        for(i = 0,s = 0,j = tmpPoints.begin();i < widthsEdits.count();s = 0, tmpW = 0, i ++, j++){
            if(i == 0 && fixedChecks[i]->checkState() == Qt::Checked){
                while(fixedChecks[i]->checkState() == Qt::Checked){
                    j = tmpPoints.insert(j + 1,(*j + widthsEdits[i]->text().toDouble()));
                    qDebug() << tmpPoints;
                    i++;
                }
            }
            else
                if(fixedChecks[i]->checkState() == Qt::Checked){
                    while((i + s) < widthsEdits.count() && fixedChecks[i + s]->checkState() == Qt::Checked){
                        tmpW += widthsEdits[i + s]->text().toDouble();
                        qDebug() << "s" << s;
                        qDebug() << "fix" <<widthsEdits[i + s]->text().toDouble();
                        s++;
                    }

                    qDebug() << "tmpW" << tmpW;

                    if(i + s < widthsEdits.count()){
                        *j = *j - tmpW/2;
                        while(fixedChecks[i]->checkState() == Qt::Checked){
                        j = tmpPoints.insert(j + 1,(*j + widthsEdits[i]->text().toDouble()));
                        qDebug() << tmpPoints;
                        i++;
                        }
                    }
                    else{
                        *j = *j - tmpW;
                        while( i < widthsEdits.count() - 1 && fixedChecks[i]->checkState() == Qt::Checked){
                            tmpPoints.append(*j + widthsEdits[i]->text().toDouble());
                            qDebug() << tmpPoints;
                            j = tmpPoints.end();
                            i++;
                        }
                        tmpPoints.append(Size);
                    }
                }
        }
    }
    else {
       equalSize = (Size - fixWidthSum)/W;

       tmpPoints.clear();
       tmpPoints.append(0);

       for(i = 0, j = tmpPoints.begin(); i < widthsEdits.count() - 1;i ++, j ++){
           if (fixedChecks[i]->checkState() == Qt::Checked)
               tmpPoints.append(*j + widthsEdits[i]->text().toDouble());
           else tmpPoints.append(*j + equalSize);
       qDebug() << "tmpPoints" << tmpPoints;
       }
       tmpPoints.append(Size);
    }


   cutPoints = tmpPoints;
   qDebug() << "cutPoints" << cutPoints;



   for(i = 0, j = tmpPoints.begin();i < widthsEdits.count(); i ++, j++)
   {
       cut(*j,*(j+1),i);
       getVolume("tmp",i);
    }


   double fixWidth = 0, delta = 0;
   double tmpPrecision = precision, tmp = 0, tmpV = 0;   //виправити. поставити справді виміряне відхилення



   for (i = widthsEdits.count() - 1;i >= 0;i --)
       if(fixedChecks[i]->checkState() != Qt::Checked){ // що якщо тільки один елемент буде з обємом, всі інші - фіксовані?
           tmpV = tmpVolume[i];
           break;
       }

   for(i = 0;i < widthsEdits.count(); i ++)

       if(fixedChecks[i]->checkState() != Qt::Checked)
       {
           tmp += fabs(tmpV - tmpVolume[i]);
           tmpV = tmpVolume[i];
           // тут тмп в присвоюється
       //обчислення відхилення від середнього
       }

   if(tmp > precision)
       tmpPrecision = tmp;


   //!!!додати що інакше - модель вже відповідає параметрам


   while(tmpPrecision > precision){

       for (i = 0, j = tmpPoints.begin(), z = tmpPoints.begin(); i < widthsEdits.count() - 1; i ++, j++) {

           if (fixedChecks[i]->checkState() == Qt::Checked) { qDebug() << i << "-------------continue------------";
               continue;
           }
           else {

               for(s = i + 1; s < widthsEdits.count();s ++)
                   if(fixedChecks[s]->checkState() != Qt::Checked){

                       z = tmpPoints.begin() + s + 1;
                       qDebug() << "z" << *z;

                       break;

                       // потрібно зробити глобальний вихід з циклу, якщо вже немає нефіксованого елемента.
                   }

               if(s == widthsEdits.count())
                   break;


               qDebug() << "s" << s;
               qDebug() << "*(j + 1)" << *(j + 1);
               qDebug() << "*(z - 1)" << *(z - 1);


               if (fixedChecks[i + 1] -> checkState() != Qt::Checked) {
                   n0 = *j;
                   n1 = *z;
                   while((delta = fabs(tmpVolume[s] - tmpVolume[i])) >  slicePrecision
                         && (n1 - n0) > 0.000001) // має ще бути обмеження по максимальній точності по ширині
                   {
                       //   qDebug() << "delta = " << delta;

                       if(tmpVolume[s] > tmpVolume[i]) n0 = *(j + 1);
                       else  n1 = *(j + 1);

                       *(j + 1) = (n1 + n0)/2; //перевірити

                       //  qDebug() << "ser" << *(j + 1);

                       cut(*j,*(j + 1),i);
                       cut(*(j + 1),*z,s);
                       getVolume("tmp", i);
                       getVolume("tmp", s);
                   }
               }

               else
               {
                   n0 = *j;
                   n1 = *z;

                   fixWidth = (*(z - 1) - *(j + 1));

                   if(tmpVolume[s] > tmpVolume[i])
                       while((delta = fabs(tmpVolume[s] - tmpVolume[i])) >  slicePrecision
                             && n1 - n0 > 0.000001) // має ще бути обмеження по максимальній точності по ширині
                       {
                           //qDebug() << "delta = " << delta;

                           if(tmpVolume[s] > tmpVolume[i]) n0 = *(z - 1);
                           else  n1 = *(z - 1);

                           *(z - 1) = (n0 + n1)/2;

                           *(j + 1) = *(z - 1) - fixWidth;

                           //qDebug() << "ser" << *(j + 2);

                           cut(*j,*(j + 1),i);
                           cut(*(z - 1),*z,s);
                           getVolume("tmp", i);
                           getVolume("tmp", s);

                       }
                   else {

                       while((delta = fabs(tmpVolume[s] - tmpVolume[i])) >  slicePrecision
                             && n1 - n0 > 0.000001) // має ще бути обмеження по максимальній точності по ширині
                       {
                           //qDebug() << "delta = " << delta;

                           if(tmpVolume[s] > tmpVolume[i]) n0 = *(j + 1);
                           else  n1 = *(j + 1);

                           *(j + 1) = (n0 + n1)/2;

                           *(z - 1) = *(j + 1) + fixWidth;


                           cut(*j,*(j + 1),i);
                           cut(*(z - 1),*z,s);
                           getVolume("tmp", i);
                           getVolume("tmp", s);

                       }
                   }
                   for(int k = i + 1; k < s - 1; k ++)
                       tmpPoints[k + 1] = tmpPoints[k] + widthsEdits[k]->text().toDouble();
               }
           }
       }


       // calculating presicion

       for (i = widthsEdits.count() - 1;i >= 0;i --)
           if(fixedChecks[i]->checkState() != Qt::Checked){ // що якщо тільки один елемент буде з обємом, всі інші - фіксовані?
               tmpV = tmpVolume[i];
               break;
           }

       tmp = 0;

       for(i = 0;i < widthsEdits.count(); i ++)

           if(fixedChecks[i]->checkState() != Qt::Checked)
           {
               tmp += fabs(tmpV - tmpVolume[i]);
               tmpV = tmpVolume[i];
               // тут тмп в присвоюється
               //обчислення відхилення від середнього
           }




       qDebug() << "tmp(precision)  " << tmp;
//       qDebug() << "sliceVolume" << sliceVolume;
       qDebug() << "tmpVolume" << tmpVolume;
//       qDebug() << "cutPoints" << cutPoints;
       qDebug() << "tmpPoints" << tmpPoints;

//       ui ->label_5->setText(QString::number(tmp));

       // перевірка, чи алгоритм збіжний і подальша його ініціалізація

       if(tmpPrecision > tmp)
       {
           tmpPrecision = tmp;
           cutPoints = tmpPoints;
           sliceVolume = tmpVolume;
           // алгоритм збіжний.
           // Будемо виконувати наступну ітерацію, поки не досягнута максимальна точність
       }
       else {
           break;
           QMessageBox::information(this, QApplication::applicationName(),
                                    "Мodel is not appropriate for the type manifol 2 or other error.");
       }





       //отут поставив перевірку, що якщо алгоритм збіжний - замінити масиви,
       //якщо ні - зупинити цикл і вивести повідомлення, що модель не дорівнює маніфол 2,
       //або є інша помилка.

   }
   //------------------------------------------------------------------------------------
   // end general calculating



   // last calculating wolume

   for(i = 0, j = cutPoints.begin();i < widthsEdits.count(); i ++, j++)
   {
       cut(*j,*(j+1),i);
       getVolume("tmp",i);
   }


   sliceVolume = tmpVolume;

   QString showVolume, showPoint;

   showPoint.clear();
   showPoint += "0   ";
   showVolume.clear();

   //show slice width
   for(i = 0, j = cutPoints.begin();i < widthsEdits.count();i ++, j ++){
       showVolume += (QString::number(sliceVolume[i]) + "   ");
       showPoint += (QString::number(*(j + 1)) + "   ");
       widthsEdits[i]->setText(QString::number(*(j + 1) - *j));
   }

   //show points cutting and slice volume
   ui->lineEdit_2->setText("Cut points:   " + showPoint);
   ui->lineEdit_3->setText("Slices volume:   " + showVolume);




   ui->calculateVolumeButton->setEnabled(true);
   ui->cutModelButton->setEnabled(true);
   ui->calculateButton->setEnabled(true);
}


//-------------------------------------------------------------------------------------------
