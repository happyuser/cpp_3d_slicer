#ifndef SLISERSETINGS_H
#define SLISERSETINGS_H

#include <QObject>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>




class SliserSettings
{
public:
    SliserSettings(QString);


    QString admeshExecutableFilePath;
    QString openscadExecutableFilePath;

    QString defualtCutAxis;     // could be either x or y
    int defualtSlices;          // number of slices
    float Z_PreCutHeight;       // new parameter , measured in mm.
    bool volumeSlice;           //configure the basic cut to be based on volume instead of width.
    float minimumSliceWidth;    // minimum width for a non fixed slice.
    float precision;

     /*Fixed slices list.  First create the array of by the required slices as defined at DefualtSlices,
      * then read settings for any fixed config if exist.*/

 //   QMap<int, float> fixSlice;                 // slice number, default width in mm

};

#endif // SLISERSETINGS_H
