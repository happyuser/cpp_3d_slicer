QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = 3dSlicer
TEMPLATE = app
windows:CONFIG += static
SOURCES += main.cpp mainwindow.cpp \
    slisersettings.cpp
HEADERS += mainwindow.h \
    slisersettings.h
FORMS   += mainwindow.ui
RESOURCES += 3dslicer.qrc
RC_FILE = 3dslicer.rc
